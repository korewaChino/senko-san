exports.run = async (senko, msg) => {
    const { prefix } = require('../../config.json');
    if (msg.author.bot) return;
    if (!msg.guild) return msg.channel.send('Sorry, but DMs aren\'t supported.');
    if (msg.content.indexOf(prefix) !== 0) return;



    let args = msg.content.slice(prefix.length).trim().split(/ +/g);
    let command = args.shift().toLowerCase();
  
    /*let aliasCmd;
    for(const aliases of Object.values(senko.settings.get(msg.guild.id, 'aliases'))) {
      if(aliases.find(key => key === command)) {
        const guildAliases = senko.settings.get(msg.guild.id, 'aliases'), cmdName = Object.keys(guildAliases).find(key => guildAliases[key] === aliases);
        aliasCmd = cmdName;
      }
    }
*/
    let cmdFile = senko.commands.get(command) || senko.commands.get(aliasCmd);
    if (!cmdFile) return;
    console.log(cmdFile);
    cmdFile.run(senko, msg, args);

        
}
