
# Senko-san
[![Discord Bots](https://discordbots.org/api/widget/363384807734116354.svg)](https://discordbots.org/bot/363384807734116354)

Discord.JS bot by Cappuchino
## Selfhosting
To selfhost make a config.json file and put this in here
```
{
    "prefix": "Yout Token here",
    "owner": "Your User ID",
    "token": "The Bot's Token",
    "mongourl": "MongoDB URI"
}
```
## Add Senko-san to your server
If you're an admin or a server owner [click on this link](https://discordapp.com/oauth2/authorize?client_id=363384807734116354&scope=bot) to add Senko-san to your server. Or click on [this](https://discordbots.org/bot/363384807734116354) to vote for it if you liked it!
## Support Server
[Join me on Discord!](http://discord.gg/KfFTecu)
