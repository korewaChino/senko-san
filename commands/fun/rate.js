exports.run = async (senko, msg, args) => {
    let target = args.join(" ")
    let rating = Math.floor(Math.random() * 10)
    if(!target) await msg.channel.send(`Who are you even rating?`);
    else msg.channel.send(`I'd give ${target} a rating of ${rating} out of 10.`)
}
exports.help = {
    name: 'rate',
    description: 'Rates something out of 10.'
}