exports.run = async (senko, msg, args) => {
    const hugs = [
        'https://media.giphy.com/media/13YrHUvPzUUmkM/giphy.gif?cid=3640f6095baba8037a38754c59e97eaa',
        'https://media.giphy.com/media/BXrwTdoho6hkQ/giphy.gif?cid=3640f6095baba8037a38754c59e97eaa',
        'https://media.giphy.com/media/iMrHFdDEoxT5S/giphy.gif?cid=3640f6095baba8037a38754c59e97eaa'
    ]
    let target = args.join(" ")
    let img = hugs[Math.floor(Math.random() * hugs.length)]
    if(!target) msg.channel.send("Who are you trying to hug?")
    else{
        msg.channel.send(`${msg.author} gave ${target} a hug! ${img}`);
    }
}

exports.help = {
    name: 'hug',
    description: 'gives hugs!'
}