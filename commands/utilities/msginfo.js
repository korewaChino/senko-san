const Discord = require("discord.js")
exports.run = async (senko,msg, args) =>{
    let targetmsg = msg.channel.fetchMessage(args[0]);
    if(!targetmsg) await msg.channel.send("`ERR:` Invalid Message ID")
    let embed = new Discord.RichEmbed
    .addField(`Created at`, targetmsg.createdAt)
    msg.channel.send(embed).catch(error)
}
exports.help = {
    name: 'msginfo',
    description: 'Shows information of the specified messasge ID'
}