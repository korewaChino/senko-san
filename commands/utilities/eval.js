const config = require("../../config.json")
exports.run = async (senko, msg, args) => {
    function clean(text) {
        if (typeof(text) === "string")
          return text.replace(/`/g, "`" + String.fromCharCode(8203)).replace(/@/g, "@" + String.fromCharCode(8203));
        else
            return text;
      }
      if (config.owner.indexOf(msg.author.id) < 0) return msg.channel.send("`ERR:` You are not the Owner!");
     else try {
      const code = args.join(" ");
      let evaled = eval(code);
 
      if (typeof evaled !== "string")
        evaled = require("util").inspect(evaled);
 
      msg.channel.send(clean(evaled), {code:"xl"});
    } catch (err) {
      msg.channel.send(`\`ERROR\` \`\`\`xl\n${clean(err)}\n\`\`\``);
    }
}
exports.help = {
    name: 'eval',
    description: 'Evaluates JS output --DEV ONLY--',
    usage: 'eval <code>'
}