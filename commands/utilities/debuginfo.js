const os = require("os");
const Discord = require("discord.js")
const senko = new Discord.Client
exports.run = async (senko, msg, args) => {
    let embed = new Discord.RichEmbed()
    .setTitle(`${senko.user.username} Host info`)
    .setAuthor(senko.user.username, senko.user.avatarURL)
    .addField(`Host CPU Info`,`Architecture:  ${os.arch()}`)
    .addField(`Host Platform`, `${os.platform()} ${os.release()} for ${os.arch()}`)
    .addField(`Host OS Type`, os.type())
    .addField(`Host RAM`, `Free RAM: ${os.freemem()} Bytes|Total RAM: ${os.totalmem()} Bytes`)
    msg.channel.send(embed)
}

exports.help = {
    name: 'debuginfo',
    description: 'Shows host information'
}